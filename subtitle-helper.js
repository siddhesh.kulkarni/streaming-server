const fs = require("fs");

function srt2webvtt(data) {
  // remove dos newlines
  var srt = data.replace(/\r+/g, "");
  // trim white space start and end
  srt = srt.replace(/^\s+|\s+$/g, "");
  // get cues
  var cuelist = srt.split("\n\n");
  var result = "";
  if (cuelist.length > 0) {
    result += "WEBVTT\n\n";
    for (var i = 0; i < cuelist.length; i = i + 1) {
      result += convertSrtCue(cuelist[i]);
    }
  }
  return result;
}

function convertSrtCue(caption) {
  // remove all html tags for security reasons
  //srt = srt.replace(/<[a-zA-Z\/][^>]*>/g, '');
  var cue = "";
  var s = caption.split(/\n/);
  // concatenate muilt-line string separated in array into one
  while (s.length > 3) {
    for (var i = 3; i < s.length; i++) {
      s[2] += "\n" + s[i];
    }
    s.splice(3, s.length - 3);
  }
  var line = 0;
  // detect identifier
  if (!s[0].match(/\d+:\d+:\d+/) && s[1].match(/\d+:\d+:\d+/)) {
    cue += s[0].match(/\w+/) + "\n";
    line += 1;
  }
  // get time strings
  if (s[line].match(/\d+:\d+:\d+/)) {
    // convert time string
    var m = s[1].match(
      /(\d+):(\d+):(\d+)(?:,(\d+))?\s*--?>\s*(\d+):(\d+):(\d+)(?:,(\d+))?/
    );
    if (m) {
      cue +=
        m[1] +
        ":" +
        m[2] +
        ":" +
        m[3] +
        "." +
        m[4] +
        " --> " +
        m[5] +
        ":" +
        m[6] +
        ":" +
        m[7] +
        "." +
        m[8] +
        "\n";
      line += 1;
    } else {
      // Unrecognized timestring
      return "";
    }
  } else {
    // file format error or comment lines
    return "";
  }
  // get cue text
  if (s[line]) {
    cue += s[line] + "\n\n";
  }
  return cue;
}

function getSubtitles(videoFilePath) {
  const filesToLookinto = ["vtt", "srt"];
  if (!videoFilePath) {
    return false;
  }

  let retSubs;
  let type;

  for (const fileToLook of filesToLookinto) {
    let lastFilePath;
    try {
      const splitted = videoFilePath.split(".");
      splitted[splitted.length - 1] = fileToLook;
      const joined = splitted.join(".");
      lastFilePath = joined;
      const fileStr = fs.readFileSync(joined, "utf8");
      if (fileStr) {
        retSubs = fileStr;
        type = fileToLook;
        break;
      }
    } catch (err) {
      if (err.code === "ENOENT") {
        console.warn(
          "No subs found with",
          fileToLook,
          "extension at",
          lastFilePath
        );
      } else {
        console.warn("err looking for subs", err);
      }
    }
  }
  if (type === "srt") {
    retSubs = srt2webvtt(retSubs);
  }
  return retSubs;
}

module.exports.srt2webvtt = srt2webvtt;
module.exports.getSubtitles = getSubtitles;
