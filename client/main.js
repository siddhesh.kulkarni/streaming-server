$(document).ready(function () {
  let userId = localStorage.getItem("vdouserId")
    ? localStorage.getItem("vdouserId")
    : null;
  let videoList = [];
  let videoId = 0;
  let hashList = {};
  let videoMetaDataLoaded = false;

  const randomStringGenerator = function randomStringGenerator() {
    var text = "";
    var possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 16; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  };

  const videoRefresh = function (currTime) {
    const video = document.getElementById("videoPlayer");
    const source = document.getElementById("videoSource");
    const captions = document.getElementById("videoCaptions");
    const vdoId = getCurrentVideoIdUsingLS();
    const randomStr = randomStringGenerator();

    if (vdoId !== undefined) {
      videoId = vdoId;
    }

    video.addEventListener(
      "loadedmetadata",
      function () {
        if (currTime !== undefined) {
          this.currentTime = currTime;
        }
        videoMetaDataLoaded = true;
      },
      false
    );
    video.addEventListener(
      "timeupdate",
      _.throttle(function () {
        setCurrentVideoProgress();
      }, 10000)
    );

    video.addEventListener("play", setCurrentVideoProgress);
    video.addEventListener("pause", setCurrentVideoProgress);

    source.setAttribute(
      "src",
      "/video?uid=" +
        userId +
        "&videoId=" +
        (videoId ? videoId : 0) +
        "&cb=" +
        randomStr
    );
    captions.setAttribute(
      "src",
      "/captions?uid=" +
        userId +
        "&videoId=" +
        (videoId ? videoId : 0) +
        "&cb=" +
        randomStr
    );
    video.load();
  };

  const onVideoSelectClick = function (ev) {
    // console.log("ev", ev);
    const target = ev.target;
    if (target.id) {
      const fileindex = Number(target.id.replace("video-", ""));

      if (!userId || fileindex === undefined || fileindex === null) {
        return;
      }

      fetch("/setVideoURL?uid=" + userId, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({ index: fileindex }),
      })
        .then(function (res) {
          setTimeout(function () {
            // setCurrentVideo(fileindex);
            location.reload();
            /* videoRefresh(); */
          }, 500);
        })
        .catch(function (res) {
          console.warn(res);
        });
    }
  };

  const init = function () {
    const listElem = [
      "<h3>Selected video: " +
        (typeof videoList[videoId] === "string"
          ? videoList[videoId].split("\\")[
              videoList[videoId].split("\\").length - 1
            ]
          : "Unknown"),
      "</h3>",
      '<p>List of videos found:</p><ul class="list-group">',
      ...videoList.map(
        (v, index) =>
          `<li class="list-group-item">${v} | <button id="video-${index}" class="btn btn-primary">Select</button></li>`
      ),
      `</ul>`,
    ];

    document.getElementById("main-container").innerHTML = listElem.join("");
    $(document).find("button").on("click", onVideoSelectClick);

    videoRefresh(getCurrTimeFromLS());
  };

  const getMetadata = function () {
    fetch("/init?uid=" + userId)
      .then((data) => data.json())
      .then(function (resp) {
        console.log("/init resp", resp);
        videoId = resp.videoId;
        userId = localStorage.getItem("vdouserId")
          ? localStorage.getItem("vdouserId")
          : resp.userId;
        localStorage.setItem("vdouserId", userId);
        videoList = resp.videoList;
        hashList = resp.hashList;
        setCurrentVideo(videoId);
        init();
      })
      .catch(console.warn);
  };

  const getCurrVideoFromLS = function () {
    let userProg = localStorage.getItem("userProgress");
    if (!userProg) {
      return;
    }
    userProg = JSON.parse(userProg);
    if (!userProg[userId]) {
      return;
    }
    return userProg[userId]["currVideo"];
  };

  const getCurrentVideoIdUsingLS = function () {
    const currVdoHashId = getCurrVideoFromLS();
    let vdoId = 0;

    if (currVdoHashId) {
      for (const path in hashList) {
        if (hashList.hasOwnProperty(path)) {
          if (currVdoHashId === hashList[path]) {
            for (let i = 0; i < videoList.length; i++) {
              if (videoList[i] === path) {
                vdoId = i;
                break;
              }
            }
            break;
          }
        }
      }
    }
    return vdoId;
  };

  const getCurrTimeFromLS = function () {
    const vdoId = getCurrentVideoIdUsingLS();
    let userProg = localStorage.getItem("userProgress");
    if (!userProg) {
      return;
    }
    userProg = JSON.parse(userProg);
    if (!userProg[userId]) {
      return;
    }
    const currVdoHashId = hashList[videoList[vdoId]];
    if (!currVdoHashId) {
      return;
    }
    if (!userProg[userId]["currTime"]) {
      return;
    }
    console.log(
      "getCurrTimeFromLS",
      currVdoHashId,
      userProg[userId]["currTime"][currVdoHashId]
    );
    return userProg[userId]["currTime"][currVdoHashId];
  };

  const setCurrentVideoProgress = function () {
    const vdoId = getCurrentVideoIdUsingLS();
    if (!videoMetaDataLoaded) {
      return;
    }
    const currTime = $("#videoPlayer")[0].currentTime;
    let userProg = localStorage.getItem("userProgress");
    const currVdoHashId = hashList[videoList[vdoId]];
    if (!userProg) {
      userProg = "{}";
    }
    userProg = JSON.parse(userProg);
    if (currVdoHashId && currTime !== undefined) {
      if (!userProg[userId]) {
        userProg[userId] = {
          currTime: {
            [currVdoHashId]: currTime,
          },
        };
      } else {
        if (!userProg[userId]["currTime"]) {
          userProg[userId]["currTime"] = {};
        }
        userProg[userId]["currTime"][currVdoHashId] = currTime;
      }
      userProg[userId]["currVideo"] = currVdoHashId;
    }
    localStorage.setItem("userProgress", JSON.stringify(userProg));
    console.log("save time", currVdoHashId, currTime);
  };

  const setCurrentVideo = function (currVideoId) {
    let vdoId = currVideoId;
    if (vdoId === undefined) {
      vdoId = getCurrentVideoIdUsingLS();
    }
    const currVdoHashId = hashList[videoList[vdoId]];
    let userProg = localStorage.getItem("userProgress");
    if (!userProg) {
      userProg = "{}";
    }
    userProg = JSON.parse(userProg);
    if (currVdoHashId) {
      if (!userProg[userId]) {
        userProg[userId] = {
          currVideo: currVdoHashId,
        };
      } else {
        userProg[userId]["currVideo"] = currVdoHashId;
      }
    }
    localStorage.setItem("userProgress", JSON.stringify(userProg));
    console.log("set current video", currVdoHashId);
  };

  $(window).on("beforeunload", function () {
    setCurrentVideoProgress();
  });

  getMetadata();
});
