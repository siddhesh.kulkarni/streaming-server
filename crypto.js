const crypto = require("crypto");

const getHash = function getHash(inputStr) {
  return crypto.createHash("md5").update(inputStr).digest("hex");
};

module.exports.getHash = getHash;
