const express = require("express");
const fs = require("fs");
const path = require("path");
const bodyParser = require("body-parser");
const { v4 } = require("uuid");
const { getHash } = require("./crypto");

const { ROOT_DIR, ROOT_DIRS } = require("./config");
const { getSubtitles } = require("./subtitle-helper");

const app = express();
app.use(bodyParser.json());

const users = {};
const _cache = {
  getWithSetter: function (key, withSetter) {
    if (_cache.get(key) !== undefined) {
      return _cache.get(key);
    }
    const out = withSetter();
    _cache.set(out);
    return out;
  },
  get: function (key) {
    return _cache[key];
  },
  set: function (key, value) {
    _cache[key] = value;
    return value;
  },
};
let videoList = [];
const hashList = {};
/* let indexFileStr = fs.readFileSync(
  path.resolve(__dirname, "client/index.html"),
  "utf8"
); */

const getFilesRecursively = (directory, extensions) => {
  if (!extensions) {
    extensions = [];
  }
  const filesInDirectory = fs.readdirSync(directory);
  let files = [];
  for (const file of filesInDirectory) {
    const absolute = path.join(directory, file);
    if (fs.statSync(absolute).isDirectory()) {
      files = [...files, ...getFilesRecursively(absolute, extensions)];
    } else {
      const extension = file.split(".")[file.split(".").length - 1];
      if (extensions.filter((f) => f === extension).length) {
        files.push(absolute);
        hashList[absolute] = getHash(absolute);
      }
    }
  }
  return files;
};

const setcache = () => {
  if (videoList.length === 0) {
    if (ROOT_DIR) {
      videoList = [
        ...videoList,
        ...getFilesRecursively(ROOT_DIR, ["mp4", "mkv", "avi", "flv"]),
      ];
    }
    if (ROOT_DIRS.length) {
      ROOT_DIRS.map((rootDir) => {
        videoList = [
          ...videoList,
          ...getFilesRecursively(rootDir, ["mp4", "mkv", "avi", "flv"]),
        ];
      });
    }
    console.log("hashList", hashList);
  }
};

app.use(express.static(__dirname + "/client"));

/* app.get("/", function (req, res) {
  setcache();

  res.setHeader("Content-Type", "text/html");
  res.status(200).send(indexFileStr);
}); */

app.get("/init", function (req, res) {
  const videoId = users[req.query.uid] !== undefined ? users[req.query.uid] : 0;
  users[req.query.uid] = videoId;
  res.status(200).send({ videoList, userId: v4(), videoId, hashList });
});

app.post("/setVideoURL", function (req, res) {
  if (req.body.index !== undefined || req.query.uid !== undefined) {
    users[req.query.uid] = req.body.index;
  }
  console.log(
    "/setVideoUrl",
    "uid",
    req.query.uid,
    "videoId",
    users[req.query.uid]
  );
  res.status(200).send("<h1>DONE</h1>");
});

app.get("clearcache", function (req, res) {
  videoList.length = 0;
  /* indexFileStr = ""; */
  res.setHeader("Content-Type", "text/html");
  res.status(200).send("<h1>DONE</h1>");
});

app.get("setcache", function (req, res) {
  setcache();
  res.setHeader("Content-Type", "text/html");
  res.status(200).send("<h1>DONE</h1>");
});

app.get("/video", function (req, res) {
  let videoPath =
    users[req.query.uid] !== undefined
      ? videoList[users[req.query.uid]]
      : videoList[0];

  if (users[req.query.uid] === undefined) {
    users[req.query.uid] = 0;
  }

  console.log("user", req.query.uid, "video", videoPath);

  if (!videoPath) {
    res.setHeader("Content-Type", "text/html");
    res.status(402).send("<h1>Try selecting a video and reload again</h1>");
    return;
  }

  const stat = fs.statSync(videoPath);
  const fileSize = stat.size;
  const range = req.headers.range;

  if (range) {
    const parts = range.replace(/bytes=/, "").split("-");
    const start = parseInt(parts[0], 10);
    const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;

    const chunksize = end - start + 1;
    const file = fs.createReadStream(videoPath, { start, end });
    const head = {
      "Content-Range": `bytes ${start}-${end}/${fileSize}`,
      "Accept-Ranges": "bytes",
      "Content-Length": chunksize,
      "Content-Type": "video/mp4",
    };

    res.writeHead(206, head);
    file.pipe(res);
  } else {
    const head = {
      "Content-Length": fileSize,
      "Content-Type": "video/mp4",
    };
    res.writeHead(200, head);
    fs.createReadStream(videoPath).pipe(res);
  }
});

app.get("/captions", function (req, res) {
  const subs = _cache.getWithSetter(`subs:${users[req.query.uid]}`, () =>
    getSubtitles(videoList[users[req.query.uid]])
  );
  if (!subs) {
    res.setHeader("Content-Type", "text/html");
    res.status(404).send("<h1>Error in Input / File not found</h1>");
    return;
  }
  res.setHeader("Content-Type", "text/plain");
  res.status(200).send(subs);
});

const bootstrapApp = function bootstrapApp() {
  const PORT = process.env.PORT ? process.env.PORT : 8000;
  app.listen(PORT, function () {
    console.log("Listening on port " + PORT);
  });
};

module.exports.bootstrapApp = bootstrapApp;
module.exports.APP = app;

setcache();
bootstrapApp();
