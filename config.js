// Edit this path to your local root dir
const isWin = process.platform === "win32";
module.exports.ROOT_DIR = "";
module.exports.ROOT_DIRS = isWin
  ? ["C:/Videos"]
  : "/User/";
