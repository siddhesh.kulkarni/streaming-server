# Streaming Server

> Stream videos from your local directory  
  
![Preview](assets/screenshot-1-preview.png "Preview")  
  
## Getting started

- With a index page to launch videos
- Supports subtitles files like .srt and .vtt
- Multiple root directories allowed
- Supports recently played video and reloads video from the previous state

### Installation / Usage

```
npm install
```

update `config.js` to specify your `ROOT_DIR` for videos.

```
npm run start // to start local server
```

OR open `run.bat` on windows machine.  
  
Now go to //localhost:8000/ OR on this address on network //192.168.1.2:8000/
